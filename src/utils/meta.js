import localize from '@/locales/localize'
export default {
  metaTitle (titleKey) {
    const appName = process.env.VUE_APP_TITLE
    return `${localize.localizeFilter(titleKey)} | ${appName}`
  }
}
