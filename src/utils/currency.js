export default {
  currencyFilter (value, currency = 'PLN') {
    return new Intl.NumberFormat('en-IN', {
      style: 'currency',
      currency
    }).format(value)
  }
}
