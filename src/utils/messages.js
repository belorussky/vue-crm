import localize from '@/locales/localize'
export default {
  logout: localize.localizeFilter('Message_Logout'),
  login: localize.localizeFilter('Message_Login')
}
