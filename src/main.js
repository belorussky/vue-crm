import { createApp } from 'vue'
import { createHead } from '@vueuse/head'
import App from './App.vue'
import { VuelidatePlugin } from '@vuelidate/core'
import './registerServiceWorker'
import router from './router'
import store from './store'
import tooltipDirective from '@/directives/tooltip.directive'
import messagePlugin from '@/utils/message.plugin'
import firebaseConfig from '@/firebase.config'
import Loader from '@/components/app/Loader'
import Pagination from 'v-pagination-3'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const app = createApp(App)
const head = createHead()
app.directive('tooltip', tooltipDirective)
app.component('Loader', Loader)
app.component('Pagination', Pagination)

firebase.initializeApp({
  apiKey: firebaseConfig.firebase().apiKey,
  authDomain: firebaseConfig.firebase().authDomain,
  databaseURL: firebaseConfig.firebase().databaseURL,
  projectId: firebaseConfig.firebase().projectId,
  storageBucket: firebaseConfig.firebase().storageBucket,
  messagingSenderId: firebaseConfig.firebase().messagingSenderId,
  appId: firebaseConfig.firebase().appId
})

let app1

firebase.auth().onAuthStateChanged(() => {
  if (!app1) {
    app1 = app.use(head).use(messagePlugin).use(VuelidatePlugin).use(store).use(router).mount('#app')
  }
})
