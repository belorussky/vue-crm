import firebase from 'firebase/app'
export default {
  actions: {
    async login ({ dispatch, commit }, { email, password }) {
      try {
        await firebase.auth().signInWithEmailAndPassword(email.value, password.value)
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    async register ({ dispatch, commit }, { email, password, name }) {
      try {
        await firebase.auth().createUserWithEmailAndPassword(email.value, password.value)
        const uid = await dispatch('getUID')
        await firebase.database().ref(`/users/${uid}/info`).set({
          bill: 5000,
          name
        })
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
    getUID () {
      const user = firebase.auth().currentUser
      return user ? user.uid : null
    },
    async logout ({ commit }) {
      await firebase.auth().signOut()
      commit('clearInfo')
    }
  }
}
