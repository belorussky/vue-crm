import { ref, computed } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { required, minValue } from '@vuelidate/validators'
import localize from '@/locales/localize'
import { useStore } from 'vuex'
export function useFormCreateCategories (props, context) {
  const title = ref('')
  const limit = ref(1)
  const store = useStore()
  const message = ref(false)

  // Validation Logics
  const rules = {
    title: { required },
    limit: { minValue: minValue(1) }
  }

  const $v = useVuelidate(rules, { title, limit })

  const localizeObj = computed(() => {
    return {
      title: localize.localizeFilter('Title'),
      create: localize.localizeFilter('Create'),
      message_validation1: localize.localizeFilter('Message_EnterCategoryTitle'),
      message_validation2: localize.localizeFilter('Message_MinValue'),
      limit: localize.localizeFilter('Limit'),
      message_natification: localize.localizeFilter('Message_CategoryCreatedSuccessfully')
    }
  })

  async function submitCreateCategoryForm () {
    $v.value.$touch()
    if ($v.value.$invalid) {
      return
    }
    const formData = {
      title: title.value,
      limit: limit.value
    }
    try {
      const category = await store.dispatch('createCategory', formData)
      title.value = ''
      limit.value = 1
      $v.value.$reset()
      message.value = true
      context.emit('created', category)
    } catch (e) {}
  }

  return { title, limit, $v, submitCreateCategoryForm, message, localizeObj }
}
