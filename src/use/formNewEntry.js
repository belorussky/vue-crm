import { ref, computed } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { required, minValue } from '@vuelidate/validators'
import localize from '@/locales/localize'
import metaInfo from '@/utils/meta'
import { useStore } from 'vuex'
export function useFormNewEntry () {
  const description = ref('')
  const amount = ref(1)
  const store = useStore()
  const select = ref(null)
  const category = ref(null)
  const message = ref(null)
  const loading = ref(true)
  const categories = ref([])
  const type = ref('outcome')
  const meta = ref({ title: computed(() => metaInfo.metaTitle('Menu_NewRecord')) })

  // Validation Logics
  const rules = {
    amount: { minValue: minValue(1) },
    description: { required }
  }

  const $v = useVuelidate(rules, { amount, description })

  const infoBill = computed(() => { return store.getters.info })
  const canCreateRecord = computed(() => {
    if (type.value === 'income') {
      return true
    }
    return infoBill.value.bill >= amount.value
  })

  const localizeObj = computed(() => {
    return {
      title_page: localize.localizeFilter('NewRecord'),
      amount: localize.localizeFilter('Amount'),
      select: localize.localizeFilter('SelectACategory'),
      message_validation1: localize.localizeFilter('Message_Description'),
      message_validation2: localize.localizeFilter('Message_MinValue'),
      description: localize.localizeFilter('Description'),
      create: localize.localizeFilter('Create'),
      message_natification1: localize.localizeFilter('Message_NewRecordCreatedSuccessfully'),
      message_natification2: localize.localizeFilter('Message_InsufficientFundsOnTheAccount'),
      no_message: localize.localizeFilter('No_CategoriesYet'),
      create_category: localize.localizeFilter('CreateNewCategory'),
      income: localize.localizeFilter('Income'),
      outcome: localize.localizeFilter('Outcome')
    }
  })

  async function submitRecordForm () {
    $v.value.$touch()
    if ($v.value.$invalid) {
      return {}
    }

    if (canCreateRecord.value) {
      try {
        await store.dispatch('createRecord', {
          categoryId: category.value,
          amount: amount.value,
          description: description.value,
          type: type.value,
          date: new Date().toJSON()
        })

        const bill = type.value === 'income' ? infoBill.value.bill + amount.value : infoBill.value.bill - amount.value

        await store.dispatch('updateInfo', { bill })
        message.value = 1
        $v.value.$reset()
        description.value = ''
        amount.value = 1
      } catch (e) {}
    } else {
      message.value = 2
    }
  }

  return { meta, description, amount, $v, submitRecordForm, select, category, loading, categories, type, message, infoBill, localizeObj }
}
