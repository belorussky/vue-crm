import { ref, computed, onMounted } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { required } from '@vuelidate/validators'
import localize from '@/locales/localize'
import { useStore } from 'vuex'
import M from 'materialize-css/dist/js/materialize.min'
import { useHead } from '@vueuse/head'
import metaInfo from '@/utils/meta'
export function useFormProfile () {
  const userName = ref('')
  const isEnLocale = ref(true)
  const store = useStore()

  // Validation Logics
  const rules = {
    userName: { required }
  }

  const $v = useVuelidate(rules, { userName })
  const info = computed(() => {
    return store.getters.info
  })

  const localizeObj = computed(() => {
    return {
      title: localize.localizeFilter('Profile'),
      name: localize.localizeFilter('Name'),
      message_validation: localize.localizeFilter('Message_EnterName'),
      button: localize.localizeFilter('Update')
    }
  })

  onMounted(() => {
    const head = { title: computed(() => metaInfo.metaTitle('Profile')) }
    useHead(head)
    userName.value = info.value.name
    isEnLocale.value = info.value.locale === 'en-US'
    setTimeout(() => {
      M.updateTextFields()
    }, 0)
  })

  async function submitProfileForm () {
    const name = userName.value
    $v.value.$touch()
    if ($v.value.$invalid) {
      return
    }
    try {
      await store.dispatch('updateInfo', {
        name,
        locale: isEnLocale.value ? 'en-US' : 'ru-RU'
      })
    } catch (e) {}
  }

  return { userName, $v, submitProfileForm, isEnLocale, localizeObj }
}
