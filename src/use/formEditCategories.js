import { ref, computed } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { required, minValue } from '@vuelidate/validators'
import localize from '@/locales/localize'
import { useStore } from 'vuex'
export function useFormEditCategories (props, context) {
  const title = ref('')
  const limit = ref(1)
  const store = useStore()
  const select = ref(null)
  const current = ref(null)
  const message = ref(false)

  // Validation Logics
  const rules = {
    title: { required },
    limit: { minValue: minValue(1) }
  }

  const $v = useVuelidate(rules, { title, limit })

  const localizeObj = computed(() => {
    return {
      title_page: localize.localizeFilter('Edit_Category'),
      title: localize.localizeFilter('Title'),
      select: localize.localizeFilter('SelectACategory'),
      message_validation1: localize.localizeFilter('Message_EnterCategoryTitle'),
      message_validation2: localize.localizeFilter('Message_MinValue'),
      limit: localize.localizeFilter('Limit'),
      update: localize.localizeFilter('Update'),
      message_natification: localize.localizeFilter('Message_CategoryUpdatedSuccessfully')
    }
  })

  async function submitEditCategoryForm () {
    $v.value.$touch()
    if ($v.value.$invalid) {
      return
    }
    try {
      const formUpdateData = {
        id: current.value,
        title: title.value,
        limit: limit.value
      }
      await store.dispatch('updateCategory', formUpdateData)
      message.value = true
      context.emit('updated', formUpdateData)
    } catch (e) {}
  }

  return { title, limit, $v, submitEditCategoryForm, select, current, message, localizeObj }
}
