import { ref, computed, onMounted } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { email, required, minLength } from '@vuelidate/validators'
import localize from '@/locales/localize'
import { useHead } from '@vueuse/head'
import metaInfo from '@/utils/meta'
import router from '@/router'
import { useStore } from 'vuex'
export function useRegisterForm () {
  const userEmail = ref('')
  const userPassword = ref('')
  const name = ref('')
  const agree = ref(false)
  const store = useStore()

  // Validation Logics
  const rules = {
    userEmail: { required, email },
    userPassword: { required, minLength: minLength(8) },
    name: { required },
    agree: { checked: v => v }
  }

  const $v = useVuelidate(rules, { userEmail, userPassword, name, agree })

  const localizeObj = computed(() => {
    return {
      title: localize.localizeFilter('HomeBookkeeping'),
      email: localize.localizeFilter('Email'),
      message_validation1: localize.localizeFilter('Message_EmailEmpty'),
      message_validation2: localize.localizeFilter('Message_EmailValid'),
      message_validation3: localize.localizeFilter('Message_EnterPassword'),
      message_validation4: localize.localizeFilter('Message_PasswordMustBe'),
      message_validation5: localize.localizeFilter('Message_CharactersNowThis'),
      message_validation6: localize.localizeFilter('Message_EnterYourName'),
      password: localize.localizeFilter('Password'),
      name: localize.localizeFilter('Name'),
      agree: localize.localizeFilter('IAgreeWithTheRules'),
      signin: localize.localizeFilter('SignIn'),
      signup: localize.localizeFilter('SignUp'),
      message_register1: localize.localizeFilter('AlreadyHaveAnAccount')
    }
  })

  onMounted(() => {
    const head = { title: computed(() => metaInfo.metaTitle('SignUp')) }
    useHead(head)
  })

  async function submitHandler () {
    $v.value.$touch()
    if ($v.value.$invalid) {
      return
    }
    const formData = {
      email: userEmail,
      password: userPassword,
      name: name.value
    }
    try {
      await store.dispatch('register', formData)
      router.push('/')
    } catch (e) {}
  }

  return { userEmail, userPassword, name, agree, $v, submitHandler, localizeObj }
}
