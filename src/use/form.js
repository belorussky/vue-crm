import { ref, computed } from 'vue'
import { useVuelidate } from '@vuelidate/core'
import { email, required, minLength } from '@vuelidate/validators'
import localize from '@/locales/localize'
import metaInfo from '@/utils/meta'
import router from '@/router'
import { useStore } from 'vuex'
export function useForm () {
  const userEmail = ref('')
  const userPassword = ref('')
  const store = useStore()
  const meta = ref({ title: computed(() => metaInfo.metaTitle('SignIn')) })

  // Validation Logics
  const rules = {
    userEmail: { required, email },
    userPassword: { required, minLength: minLength(8) }
  }

  const $v = useVuelidate(rules, { userEmail, userPassword })

  const localizeObj = computed(() => {
    return {
      title: localize.localizeFilter('HomeBookkeeping'),
      email: localize.localizeFilter('Email'),
      message_validation1: localize.localizeFilter('Message_EmailEmpty'),
      message_validation2: localize.localizeFilter('Message_EmailValid'),
      message_validation3: localize.localizeFilter('Message_EnterPassword'),
      message_validation4: localize.localizeFilter('Message_PasswordMustBe'),
      message_validation5: localize.localizeFilter('Message_CharactersNowThis'),
      password: localize.localizeFilter('Password'),
      signin: localize.localizeFilter('SignIn'),
      message_register1: localize.localizeFilter('DontHaveAnAccount'),
      message_register2: localize.localizeFilter('RegisterNow')
    }
  })

  async function submitLoginForm () {
    $v.value.$touch()
    if ($v.value.$invalid) {
      return
    }
    const formData = {
      email: userEmail,
      password: userPassword
    }
    try {
      await store.dispatch('login', formData)
      router.push('/')
    } catch (e) {}
  }

  return { meta, userEmail, userPassword, $v, submitLoginForm, localizeObj }
}
